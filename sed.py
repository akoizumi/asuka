# sed.py module
from PythonSed import Sed
import re

sed_parse = re.compile("(?<!\\\\)[/#]")
sed_cmd = re.compile("^s[/#].*[/#].*[/#]")

def sed_command(self, msg, sender, mtype):
    try:
        text = msg["body"]
        if not sed_cmd.match(text):
            self.messages[sender]["messages"].add(Text)
            return
        sed_args = sed_parse.split(Text)

        if len(sed_args) < 4:
            return
        
        sed = Sed()
        sed.load_string(text)

        for message in self.messages[sender]["messages"]:
            msg = io.StringIO(message)
            if res := sed.apply(msg, None):
                out = "\n".join(res)
                self.messages[sender]["messages"].add(out)
                return self.send_message (
                        mto=sender,
                        mbody=res,
                        mtype=mtype,
                    )
