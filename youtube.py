# youtube.py module

import random
import youtube_dl

invidious_instances = [ "vid.puffyan.us", "youtube.076.ne.jp" ]
youtube_links = [ "www.youtube.com", "m.youtube.com" ]
youtube_link = "youtu.be"
ydl = youtube_dl.YoutubeDL()

def get_youtube_title(url):
	try:
		info = ydl.extract_info(url, download=False)
		return info["title"]
	except Exception:
		return "Unexpected error"

def get_invidious_link(yurl):
	video = yurl.split("/")[-1]
	instance = random.choice(invidious_instances)
	return f"https://{instance}/watch?v={video}&local=true"

def get_yurl(path):
	yurl = f"https://youtu.be/{path}"
	return yurl

def send_youtube_info(self, url, sender, mtype):
	if uri.netloc == youtube_link:
		yurl = get_yurl(uri.path)
	elif "v" in (query := parse_qs(uri.query)):
		if v := query["v"]:
			yurl = get_yurl(v[0])
		else:
			return
		if output := get_youtube_title(yurl):
			if output in self.messages[sender]["previews"]:
				return
			self.messages[sender]["previews"].add(output)

			invidious = get_invidious_link(yurl)
			self.send_message(mto=sender, mbody=f"*{output}*", mtype=mtype)
			self.send_message(mto=sender, mbody=invidious, mtype=mtype)
